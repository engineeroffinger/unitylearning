using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MyClass
{
    public int age1;
    public string name1;
}
public class Test : MonoBehaviour
{
    private int i1;

    protected string name2;

    [SerializeField]
    private int i2;

    [HideInInspector]
    public bool Enabled;

    public int[] arr;


    public List<int> list;

    public GameObject gameObj;
    
    public MyClass myClass;

    #region 知识点七 一些辅助特性
    //1.分组说明特性Header
    //为成员分组
    //Header特性
    //[Header("分组说明")]
    [Header("基础属性")]
    public int age;
    public bool sex;
    [Header("战斗属性")]
    public int atk;
    public int def;

    //2.悬停注释Tooltip
    //为变量添加说明
    //[Tooltip("说明内容")]
    [Tooltip("闪避")]
    public int miss;

    //3.间隔特性 Space()
    //让两个字段间出现间隔
    //[Space()]
    [Space()]
    public int crit;

    //4.修饰数值的滑条范围Range
    //[Range(最小值, 最大值)]
    [Range(0, 10)]
    public float luck;

    //5.多行显示字符串 默认不写参数显示3行
    //写参数就是对应行
    //[Multiline(4)]
    [Multiline(5)]
    public string tips;

    //6.滚动条显示字符串 
    //默认不写参数就是超过3行显示滚动条
    //[TextArea(3, 4)]
    //最少显示3行，最多4行，超过4行就显示滚动条
    [TextArea(3, 4)]
    public string myLife;

    //7.为变量添加快捷方法 ContextMenuItem
    //参数1 显示按钮名
    //参数2 方法名 不能有参数
    //[ContextMenuItem("显示按钮名", "方法名")]
    [ContextMenuItem("重置钱", "Test1")]
    public int money;
    private void Test1()
    {
        money = 99;
    }

    //8.为方法添加特性能够在Inspector中执行
    //[ContextMenu("测试函数")]
    [ContextMenu("哈哈哈哈")]
    private void TestFun()
    {
        print("测试方法");
    }
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
